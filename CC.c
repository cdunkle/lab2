#include <stdio.h>

//Our function that will prompt the user and return a valid starting number
//It contains a loop to ensure the number is positive
int getStart()     
{
    int input; //our input variable
    
    //Our loop to ensure that the number is positive
    for (int i = 0; i < 2; i++)
    {
        printf("Please enter a positive number: ");
        scanf("%d", &input);
        if (input > 0)
        {
            //Debugging
            //printf("no problems\n");
            return input;
        }
        //Loops back to ask for input again
        else
        {
            i = 0;
        }
    }
    //Just some debugging in case something goes wrong
    printf("something might have gone wrong");
    return 0;
}

//This function will return the next number based on whether it is even or odd
int nextCollatz(int x)
{
    if (x % 2 == 0)
    {
        return x/2;
    }
    else
    {
        return ((x * 3) + 1);
    }
}

//Our main funtion that does the "heavy lifting"
int main()
{
    int length = 1;
    int startNum = getStart();
    int counter = startNum;
    //prints the starting number
    printf("%d, ", startNum);
    //Loops the counter through the nextCollatz function until it equals 1
    while (counter != 1)
    {
        counter = nextCollatz(counter);
        length ++;
        printf("%d, ", counter);
    }
    printf("\nLength: %d\n", length);
}
