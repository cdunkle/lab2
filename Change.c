#include <stdio.h>

int bills(int x)
{
    int billCounter = 0;
    while(x >= 100)
    {
        x = x - 100;
        billCounter++;
        //printf("100 Has been subtracted\n");
    }
    while (x >= 20)
    {
        x = x - 20;
        billCounter++;
        //printf("20 has been subtracted\n");
    }
    while(x >= 10)
    {
        x = x - 10;
        billCounter++;
        //printf("10 has been subtracted\n");
    }
    while(x >= 5)
    {
        x = x - 5;
        billCounter++;
        //printf("5 has been subtracted\n");
    }
    while (x >= 1)
    {
        x = x - 1;
        billCounter++;
        //printf("1 has been subtracted\n");
    }
    return billCounter;
}

//Prompts the user for input and will verify that it is positive
int getAmount()
{
    int input;
    for (int i = 0; i < 10; i++)
    {
        printf("Please enter a dollar amount: ");
        scanf("%d", &input);
        if(input > 0)
        {
            return input;
        }
        else
        {
            i = 0;
        }
    }
    //This shouldn't be run ever
    printf("something went wrong");
    return 0;
}

//Our main function that just is used to run our other functions and
//Also prints out how many bills we get back
int main()
{
    int start = getAmount();
    int final = bills(start);
    printf("You get %d bills back.\n", final);
}
