#include <stdio.h>
#include <math.h>

//This is our sigmoid function that will take a float and return a float
float sigmoid(float x)
{
    return (1/(1+(exp(-x))));
}

//Our main
int main()
{
    //Our loop to go through and print outputs of the loop / sigmoid function
    for (int x = -4; x < 5; x++)
    {
        printf("%d.000 %.3f\n", x, sigmoid(x));
    }
}